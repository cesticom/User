<?php namespace Modules\User\Services;

use Modules\Core\Contracts\Authentication;
use Modules\User\Events\UserHasRegistered;
use Modules\Subscription\Events\SubscriptionIsPlanTrial;
use Modules\User\Repositories\RoleRepository;
use Carbon\Carbon;
use Modules\Alertas\Services\Alerta;

class UserRegistration
{
    /**
     * @var Authentication
     */
    private $auth;
    /**
     * @var RoleRepository
     */
    private $role;
    /**
     * @var array
     */
    private $input;

    private $alerta;

    public function __construct(Authentication $auth, RoleRepository $role, Alerta $alerta)
    {
        $this->auth = $auth;
        $this->role = $role;
        $this->alerta = $alerta;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function register(array $input)
    {
        $this->input = $input;

        $user = $this->createUser();

        if ($this->hasProfileData()) {
            $this->createProfileForUser($user);
        }

        $this->assignUserToUsersGroup($user);

        event(new UserHasRegistered($user));
    
        $this->alerta->to($user->id)->push('Complete su perfil', 'Complete su perfil', 'fa fa-hand-peace-o text-green', null, 'es', 20); 
        
        $this->alerta->to($user->id)->push('Complete su perfil', 'Complete your profile', 'fa fa-hand-peace-o text-green', null, 'en', 20);         

        return $user;
    }

    /**
     * @param array $input
     * @return mixed
     */
     public function registerTrial(array $input,$planId)
     {
         $this->input = $input;
 
         $user = $this->createUser();
 
         if ($this->hasProfileData()) {
            $profile=$this->createProfileForUser($user);
            $subscription=$this->createSuscriptionForUser($profile,$planId);
         }
 
         $this->assignUserToUsersGroup($user);
 
         event(new SubscriptionIsPlanTrial($subscription));

         $this->alerta->to($user->id)->push('Afiliacion Trial', 'Dispones de 30 días gratis', 'fa fa-hand-peace-o text-green', null, 'es', 21); 
         
         $this->alerta->to($user->id)->push('Afiliacion Trial', 'You have 30 days free', 'fa fa-hand-peace-o text-green', null, 'en', 21); 
  
         return $user;
     }
 
    private function createUser()
    {
        return $this->auth->register((array) $this->input);
    }

    private function assignUserToUsersGroup($user)
    {
        $role = $this->role->findByName('User');

        $this->auth->assignRole($user, $role);
    }

    /**
     * Check if the request input has a profile key
     * @return bool
     */
    private function hasProfileData()
    {
        return isset($this->input['profile']);
    }

    /**
     * Create a profile for the given user
     * @param $user
     */
    private function createProfileForUser($user)
    {
        $profileData = array_merge($this->input['profile'], ['user_id' => $user->id]);
        return app('Modules\Profile\Repositories\ProfileRepository')->create($profileData);
    }

    /**
     * Create a profile for the given user
     * @param $user
     */
     private function createSuscriptionForUser($profile,$planId)
     {  
        $started_at = Carbon::now()->toDateTimeString();
        
        $ended_at = Carbon::now()->addMonth();
        
        $suscriptionData = array_merge(['profile_id' => $profile->id,'plan_id'=>$planId,'started_at'=>$started_at ,'ended_at'=> $ended_at]);
         
        return app('Modules\Subscription\Repositories\SubscriptionRepository')->create($suscriptionData);  
     }
}
